var userApp = angular.module('UserApp', ['ngRoute', 'btford.socket-io'])

angular.module('UserApp')

  .config(['$routeProvider',
    function config ($routeProvider) {
      $routeProvider
        .when('/users', {
          template: '<user-list></user-list>'
        })
        .when('/users/:userId', {
          template: '<user-detail></user-detail>'
        })
        .when('/create', {
          templateUrl: 'src/CreateUser/CreateUser.html',
          controller: 'CreateUserCtrl'
        })
        .when('/realtime/:userName', {
          templateUrl: 'src/PokemonRealtime/PokemonRealtime.html',
          controller: 'PokemonRealtimeCtrl'
        })
        .otherwise({
          redirectTo: '/'
        })
    }
  ])

  .factory('mySocket', function (socketFactory) {
    var myIoSocket = io.connect('https://netology-socket-io.herokuapp.com/')

    mySocket = socketFactory({
      ioSocket: myIoSocket
    })

    return mySocket
  })
