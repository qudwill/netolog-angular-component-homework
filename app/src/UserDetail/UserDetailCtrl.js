'use strict'

userApp.component('userDetail', {
  controller: function UserDetailCtrl($routeParams, UsersService) {
    this.userLoaded = false

    var userData = UsersService.getUser($routeParams['userId']).then(function(response) {
      this.user = response.data
      this.userLoaded = true
    }.bind(this))

    this.deleteUser = function(userId) {
      this.deletionError = false
      this.deletionSuccess = false

      UsersService.deleteUser(userId).then(function successCallback(response) {
        this.deletionSuccess = true
      }.bind(this), function errorCallback(response) {
        this.deletionError = true
      }.bind(this))
    }
  },
  templateUrl: './src/UserDetail/UserDetail.html'
});