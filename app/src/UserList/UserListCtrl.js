'use strict'

userApp.component('userList', {
  controller: function UserListCtrl(UsersService, PostsService) {
    UsersService.getUsers().then(function (response) {
      this.users = response.data
    }.bind(this))

    PostsService.getPosts().then(function (response) {
      this.posts = response.data
    }.bind(this))
  },
  templateUrl: './src/UserList/UserList.html'
})